Feature: Job Offers experience
  In order to get employees
  As a job offerer
  I want to indicate experience required when creating an offer

  Background:
    Given I am logged in as job offerer

  Scenario: Create new offer with valid experience
    Given I access the new offer page
    When I fill the title with "Ruby dev"
    And I fill the experience with "5"
      And confirm the new offer
    Then I should see "Offer created"
    And I should see "Ruby dev" in My Offers
    And I should see "5" in experience of that offer

  Scenario: Create new offer with invalid experience
    Given I access the new offer page
    When I fill the title with "Ruby dev"
    And I fill the experience with "-5"
    And confirm the new offer
    Then I should see "Experience must be a number equal or greater than zero"

  Scenario: Create new offer without defined experience
    Given I access the new offer page
    When I fill the title with "Ruby dev"
    And confirm the new offer
    And I should see "Ruby dev" in My Offers
    Then I should see "Not specified" in experience of that offer