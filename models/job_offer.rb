class JobOffer
  include ActiveModel::Validations

  EXPERIENCE_TIME_PERIOD = 'years'.freeze
  EXPERIENCE_UNSPECIFIED_MSG = 'Not specified'.freeze

  attr_accessor :id, :user, :user_id, :title,
                :location, :description, :experience,
                :is_active, :updated_on, :created_on

  validates :title, presence: true
  validates :experience, numericality: { greater_than_or_equal_to: 0,
                                         message: 'Experience must be 0 or greater' }

  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
    @experience = if data[:experience].nil?
                    0
                  else
                    data[:experience]
                  end
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end

  def experience_to_show
    return EXPERIENCE_UNSPECIFIED_MSG if @experience.zero?

    @experience.to_s + ' ' + EXPERIENCE_TIME_PERIOD
  end
end
